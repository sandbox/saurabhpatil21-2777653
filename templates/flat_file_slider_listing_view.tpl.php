<?php
global $base_url;
$imgpath = variable_get('flat_file_slider_images_path');

if($delete_id > 0) {
	flat_file_slider_slider_deleteslide($delete_id);
	?>
	<div class="messages status">
	<h2 class="element-invisible">Status message</h2>
	Slide deleted successfully!!!
	</div>
	<?php
}
	
$slider = flat_file_slider_get_slides();
$html = "";
if($status=="added"){
	?>
	<div class="messages status">
	<h2 class="element-invisible">Status message</h2>
	Slide added successfully!!!
	</div>
	<?php 
}
else if($status=="updated"){
	?>
	<div class="messages status">
	<h2 class="element-invisible">Status message</h2>
	Slide updated successfully!!!
	</div>
	<?php 
}
?>
<div class='wrap'>
	<h1>Flat file slider </h1>
</div>
<form name='form1' id="form1" method='post'>
	<table>
		<thead>
			<tr>
				<th><?php echo t('Slide Image');?></th>
				<th><?php echo t('Slide Text');?></th>
				<th><?php echo t('Position');?></th>
				<th><?php echo t('Action');?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			for ($i = 0; $i < count($slider)-1; $i++) {			?>
			<tr>
				<td>
					<img height='150' width='250' src='<?php echo $base_url."/".$imgpath. '/' . $slider[$i]['image_name']; ?>' />
				</td>		
				<td><?php echo t($slider[$i]['slide_text']);?></td>
				<td><?php echo t($slider[$i]['slide_position']);?></td>
				<td>
					<div class='edit-link' id='<?php echo $slider[$i]['slide_id'];?>'>Edit</div> 
					/ <div class='link' id='<?php echo $slider[$i]['slide_id'];?>'>Delete</div> 
				</td>	
			</tr>
			<?php
			}?>
			<input type="hidden" name="delete_slide_id" id="delete_slide_id" value="" />
		</tbody>
	</table>
</form>

<form name='form2' action="slider/add-slide/" id="form2" method="post">
	<input type="hidden" name="edit_slide_id" id="edit_slide_id" value="">
</form>

<script type="text/javascript">

var $j = jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery(".link").click(function(){
		jQuery("#delete_slide_id").val(jQuery(this).attr("id"));
		jQuery("#form1").submit();
	});
	jQuery(".edit-link").click(function(){
		jQuery("#edit_slide_id").val(jQuery(this).attr("id"));
		jQuery("#form2").submit();
	});
});
</script>
<style>
.link{ cursor:pointer; color:blue}
.edit-link{ cursor:pointer; color:blue}
</style>
