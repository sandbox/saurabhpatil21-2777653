<?php
global $base_url;
$imgpath = variable_get('images_path');

	$slider = flat_file_slider_get_slides();
    for ($i = 0; $i < count($slider) - 2; $i++) {
        for ($j = $i + 1; $j < count($slider)-1; $j++) {
            if ($slider[$i]['slide_position'] > $slider[$j]['slide_position']) {
                $temp = $slider[$i];
                $slider[$i] = $slider[$j];
                $slider[$j] = $temp;
				$slider[$i]['slide_text'];
            }
        }
    }
    ?>
    <div id="slider-slider-id" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner" role="listbox">
            <?php
            
            $slide_number = variable_get('no_of_slides');
            if ($slide_number == '') {
                $slide_number = count($slider)-1;
            }
            if ($slide_number > count($slider)-1) {
                $slide_number = count($slider)-1;
            }

            for ($i = 0; $i < $slide_number; $i++) {
                ?>
                <div class="item <?php echo $i == 0 ? 'active' : ''; ?>">
                    <img src='<?php echo $base_url."/".$imgpath. '/' . $slider[$i]['image_name']; ?>' />

                    <div class="carousel-caption">
        <?php if (isset($slider[$i]['slide_text'])) { ?>
                            <h2 class="slider-title-sm"><?php echo t($slider[$i]['slide_text']); ?></h2>
                        <?php } ?>
                    </div>
                </div> <!--  items -->
        <?php
    }
    //end while
    ?>
        </div>   <!--  carousel-inner -->
            <?php /*if (count($slides) > 1) { ?>
            <ol class="carousel-indicators">
            <?php foreach ($slides as $key => $slide) { ?>
                    <li data-target="#slider-slider-id" data-slide-to="<?php echo esc_attr($key); ?>" <?php echo $key == 0 ? 'class="active"' : ''; ?>></li>
                <?php } ?>
            </ol>
            <?php }*/ ?>

        <a class="left carousel-control" href="#slider-slider-id" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#slider-slider-id" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>


    </div>