<?php
global $base_url;
drupal_add_js(drupal_get_path('module', 'flat_file_slider') . '/js/bootstrap-carousel.js');
$imgpath = variable_get('flat_file_slider_images_path');

	$slider = flat_file_slider_get_slides();
    for ($i = 0; $i < count($slider) - 2; $i++) {
        for ($j = $i + 1; $j < count($slider)-1; $j++) {
            if ($slider[$i]['slide_position'] > $slider[$j]['slide_position']) {
                $temp = $slider[$i];
                $slider[$i] = $slider[$j];
                $slider[$j] = $temp;
				$slider[$i]['slide_text'];
            }
        }
    }
    ?>

  <!-- Syntax Highlighter -->
  <link href="<?php echo drupal_get_path('module', 'flat_file_slider');?>/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo drupal_get_path('module', 'flat_file_slider');?>/css/shThemeDefault.css" rel="stylesheet" type="text/css" />
  <!-- Demo CSS -->
	
	<link rel="stylesheet" href="<?php echo drupal_get_path('module', 'flat_file_slider');?>/css/flexslider.css" type="text/css" media="screen" />

	<!-- Modernizr -->
  <script src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/modernizr.js"></script>

  <style type="text/css">
    .flex-caption {
      width: 96%;
      padding: 2%;
      left: 0;
      bottom: 0;
      background: rgba(0,0,0,.5);
      color: #fff;
      text-shadow: 0 -1px 0 rgba(0,0,0,.3);
      font-size: 14px;
      line-height: 18px;
    }
    li.css a {
      border-radius: 0;
    }
  </style>
	<section class="slider">
	<div class="flexslider">
	  <ul class="slides">
            <?php
            
            $slide_number = variable_get('flat_file_slider_no_of_slides');
            if ($slide_number == '') {
                $slide_number = count($slider)-1;
            }
            if ($slide_number > count($slider)-1) {
                $slide_number = count($slider)-1;
            }

            for ($i = 0; $i < $slide_number; $i++) {
                ?>
				
				<li>
			<img src="<?php echo $base_url."/".$imgpath. '/' . $slider[$i]['image_name']; ?>" />
		  <p class="flex-caption"><?php echo t($slider[$i]['slide_text']); ?></p>
			</li>
        <?php
    }
    //end while
    ?>
  </ul>
	</div>
	</section>
      

  <!-- jQuery -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>

  <!-- FlexSlider -->
  <script defer src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "slide"
      });
    });
  </script>


  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/jquery.easing.js"></script>
  <script src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo drupal_get_path('module', 'flat_file_slider');?>/js/demo.js"></script>
