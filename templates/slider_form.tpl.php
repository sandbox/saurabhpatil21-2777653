<?php

	$imgpath = variable_get('flat_file_slider_images_path');
	$empty_name = "";
	$error_image = "";
	$valid_exts = array('jpeg', 'jpg', 'png', 'gif');
	if(isset($_POST['file_id'])){
		if($_POST['edit_id']!==""){
			$slider = flat_file_slider_get_slides();
			flat_file_slider_replace_line($slider, (int) $_POST['edit_id']);
			drupal_goto("admin/slider/2");
		} 
		else if(!empty($_POST['add_text'])){
			foreach ($_FILES as $image) {
				// if a files was upload   if ($image['size']) {     // if it is an image    
				if (preg_match('/(jpg|jpeg|png|gif)$/', $_FILES['select_file']['type'])) {
					$file_new_name = time().$image['name'];
					$file_desti = $imgpath."/".$file_new_name;
					list($w, $h) = getimagesize($_FILES['select_file']['tmp_name']);
					$imgString = file_get_contents($_FILES['select_file']['tmp_name']);
					$image = imagecreatefromstring($imgString);
					$tmp = imagecreatetruecolor(variable_get('flat_file_slider_images_width'), variable_get('flat_file_slider_images_height'));
					imagecopyresampled($tmp, $image,0,0,0,0,variable_get('flat_file_slider_images_width'), variable_get('flat_file_slider_images_height'),$w, $h);

					switch ($_FILES['select_file']['type']) {
						case 'image/jpeg':
							imagejpeg($tmp, $file_desti, 100);
							break;
						case 'image/png':
							imagepng($tmp, $file_desti, 0);
							break;
						case 'image/gif':
							imagegif($tmp, $file_desti);
							break;
					}
					flat_file_slider_slider_addslide($file_new_name,$_POST['select_order'],$_POST['file_id'],$_POST['add_text']);			
					drupal_goto("admin/slider/1");
				}
				else {
					$error_image = "Please add correct format of image";
				}
			}
		}
		else{	
			$empty_name = "Please add text to slider";
		}
	}
	if(isset($edit_id)){
		$title = "Edit";
		global $base_url;
		$sliderArr = flat_file_slider_get_slides();
		foreach ($sliderArr as $singlearr) {
			if ($singlearr['slide_id'] == $edit_id) {
				$thisSlideId = $edit_id;
				$thisSlideCaption = $singlearr['slide_text'];
				$thisSlidePic = $singlearr['image_name'];
				$thisSlidePOs = $singlearr['slide_position'];
			}
		}
	}
		
	else{
		$title = "Add";
		$edit_id = null;
		$thisSlideId = null;
		$thisSlideCaption = null;
		$thisSlidePic = null;
		$thisSlidePOs = null;
	}
?>
<h1><?php echo $title.$edit_id; ?> slider Form</h1>
<?php
	if($error_image !="" || $empty_name !=""){?>
		<div class="messages error">
			<h2 class="element-invisible">Error message</h2>
			<?php echo $error_image;?><br>
			<?php echo $empty_name;?>
		</div>
	<?php
	}
	?>
<form method='post' class='form-item' name='test_form' id='test_form' enctype='multipart/form-data'>
	<table>
		
	<?php if(isset($edit_id)){
		?>
		<tr>
			<td><label>Previous slide image : </label></td>
			<td>
				<img height='150' width='250' src='<?php echo $base_url."/".$imgpath. '/' .$thisSlidePic; ?>' />
			</td>	
		</tr>
	<?php
			}?>
		<tr>
			<td><label>Upload Image : </label></td>
			<?php 
			if($error_image !="") {?>
				<td><input type='file' class='error' name='select_file' />
			<?php
			} else{ 
			?> 
				<td><input type='file' name='select_file' />
			<?php 
			} ?>
		</tr>
		<tr>
			<td><label>Caption : </label>
			<?php
			if($empty_name !="") { ?>
				<td><input type='text' name='add_text' maxlength='30' value="<?php echo $thisSlideCaption;?>" class='form-text error'>
			<?php
			} else { ?>
				<td><input type='text' name='add_text' maxlength='30' class='form-text'  value="<?php echo $thisSlideCaption;?>">
			<?php
			} ?>
		</tr>
		<tr>
			<td><label>Slide Position : </label>
			<?php
			$slider = flat_file_slider_get_slides();
			$total_slides = 1;
			if (count($slider) > 0) {
				$total_slides = count($slider);
			} ?>
			<td>
				<select name="select_order" class="form-select">
					<?php
					for($k = 1; $k <= $total_slides; $k++) { ?>
						<option <?php echo ($k==$thisSlidePOs)?"selected='selected'":""; ?> value="<?php echo $k;?>"><?php echo $k;?></option>
					<?php
					} ?>
				</select>
				<input type="hidden" name="file_id" value="<?php echo $total_slides;?>">
				<input type="hidden" name="edit_id" value="<?php echo $edit_id;?>">
			</td>
		</tr>
		<tr>
			<td align="right">
				<input type="submit" value="<?php echo ($edit_id!==null)?"Save":"Add"; ?>" class="form-submit">
			</td>
			<td align="left">
				<input type="reset" value="reset" class="form-submit">
			</td>
		</tr>
	</table>
</form>