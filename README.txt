CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------
Twitter Bootstrap based professional WordPress carousel slider plugin.
This Plugin is used as a flat file slider - users can Add/Edit/Delete slides and change their display position 
To fetch the slider at front end  use shortcode as - echo do_shortcode('[flat-slider]');


REQUIREMENTS
------------
This module requires the following modules:

 * Bootstrap (https://www.drupal.org/project/bootstrap)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.

 
CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.
